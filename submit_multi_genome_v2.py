# -*- coding: utf-8 -*-
"""
Created on Wed Jul 10 09:09:11 2019

@author: kruis015
"""
import argparse
import os
import subprocess
import sys
import math
parser = argparse.ArgumentParser(description='pipeline for multi genome alignment')
parser.add_argument("--seqfile", help="sequence file in progressivecactus format (mandatory)", type=str)
parser.add_argument("--prefix", help="output prefix (mandatory)", type=str)
parser.add_argument("--split_in_this_amount", help="split maf merging in this amount of jobs (default: %(default)d)", type=int, default=25)
parser.add_argument("--alignment_length", help="filter out alignments shorter than this number (default: %(default)d)", type=int, default=500)
args = parser.parse_args()


cwd = os.getcwd()

ref_species = ''
ref_fasta = ''
others = {}
with open(args.seqfile) as f:
    for line in f:
        try:
            species, file = line.strip().split()
            if line[0] == '*':
                ref_species = species.strip('*')
                ref_fasta = file
            else:
                others[species] = file
        except ValueError:
            pass

if ref_species == '':
    print('please indicate your reference species with a * before the species name')
    sys.exit()

jobcounter = 0

jobids = []
for species in others: #launch alignment jobs
    cmd = 'sbatch MUMmer.sh -r %s -q %s -l %s -o %s' % (ref_fasta, others[species], args.alignment_length, species+'_to_'+ref_species)
    jobstr = subprocess.check_output(cmd, shell=True)
    jobids.append(jobstr.strip().split()[-1])

jobcounter += len(jobids)

depstr = 'afterok'
for jobid in jobids:
    depstr += ':'+jobid

cmd2 = 'sbatch --dependency=%s split_maf.sh -n %s -r %s' % (depstr, args.split_in_this_amount, ref_species)
jobstr = subprocess.check_output(cmd2, shell=True)
jobid2 = jobstr.strip().split()[-1]
jobcounter += 1

jobids3 = []
#join maf files:
for j in range(args.split_in_this_amount):
    cmd3 = 'sbatch --dependency=afterok:%s join_maf.sh -i %s -o %s -r %s' % (jobid2, 'splitmaf_'+str(j)+'.maf', 'splitmaf_'+str(j)+'_rewritten.maf', ref_species)
    jobstr = subprocess.check_output(cmd3, shell=True)
    jobids3.append(jobstr.strip().split()[-1])
jobcounter += len(jobids3)

#finally merge results
depstr = 'afterok'
for jobid in jobids3:
    depstr += ':'+jobid
cmd2 = 'sbatch --dependency=%s merge_mafs.sh -p %s -r %s -s %s' % (depstr, args.prefix, ref_species, args.seqfile)
subprocess.call(cmd2, shell=True)
jobcounter += 1

print('submitted %s jobs to slurm' % jobcounter)
