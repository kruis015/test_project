#!/bin/bash
#SBATCH --job-name=splitmaf
#SBATCH --time=1-0
#SBATCH --mem=8000
#SBATCH --tmp=1000
#SBATCH --constraint=gen2
#SBATCH --output=split_%j.txt
#SBATCH --error=error_output_split_%j.txt

while getopts "n:r:" opt; do
  case $opt in
    r)
	REF=$OPTARG
      ;;
    n)
	NUMJOBS=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" 
      ;;
  esac
done

python split_maf.py --ref $REF --numjobs $NUMJOBS

