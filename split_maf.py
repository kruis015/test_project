# -*- coding: utf-8 -*-
"""
Created on Thu Jul 11 16:28:27 2019

@author: kruis015
"""

import sys
sys.path.append('/lustre/backup/WUR/ABGC/kruis015/scripts')
from functions import *
import subprocess
import argparse

parser = argparse.ArgumentParser(description='rewrite two maf files that are concatenated and sorted (based on maf-join)')
parser.add_argument("--ref", help="name of reference species")
parser.add_argument("--numjobs", help="amount of available jobs", type=int)
args = parser.parse_args()

def mindict(d):
    rev = {}
    for p in d:
        rev[d[p]] = p
    minimum = min([x for x in d.values()])
    return rev[minimum]

def divide_work(maffile, refspecies, num_processes):
    '''divide the work between processes, based on scaffold lengths'''
    scafs = {}
    for block in parse_maf(maffile, refspecies):
        if block.reference.contig not in scafs:
            scafs[block.reference.contig] = block.reference.contiglength
    processes = {}
    counter = {}
    for i in range(num_processes):
        processes[str(i)] = []
        counter[str(i)] = 0
    for s in scafs:
        processes[mindict(counter)].append(s)
        counter[mindict(counter)] += scafs[s]
    return processes

cmd1 = 'grep -hv "#" *%s.maf >> alltogether.maf' % args.ref
subprocess.call(cmd1, shell=True)

cmd2 = './maf-sort alltogether.maf > alltogether_sorted.maf'
subprocess.call(cmd2, shell=True)

cmd3 = 'rm alltogether.maf'
subprocess.call(cmd3, shell=True)

processes = divide_work('alltogether_sorted.maf', args.ref, args.numjobs)
for p in processes:
    alignments = parse_maf('alltogether_sorted.maf', args.ref, select=processes[p])
    write_maf(alignments, 'splitmaf_'+str(p)+'.maf')
    
cmd4 = 'rm alltogether_sorted.maf'
subprocess.call(cmd4, shell=True)
