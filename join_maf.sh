#!/bin/bash
#SBATCH --job-name=joinmaf
#SBATCH --time=4-0
#SBATCH --mem=8000
#SBATCH --tmp=1000
#SBATCH --constraint=gen2
#SBATCH --output=joinmaflog_%j.txt
#SBATCH --error=error_output_joinmaflog_%j.txt

while getopts "i:o:r:" opt; do
  case $opt in
    r)
	REF=$OPTARG
      ;;
    i)
	INFILE=$OPTARG
      ;;
    o)
	OUT=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" 
      ;;
  esac
done

python join_mafs.py --ref $REF --infile $INFILE --out $OUT 

