#!/bin/bash
#SBATCH --job-name=merge_mafs
#SBATCH --time=100
#SBATCH --mem=4000
#SBATCH --tmp=1000
#SBATCH --constraint=gen2
#SBATCH --output=anctest_%j.txt
#SBATCH --error=error_output_anctest_%j.txt

while getopts "p:s:r:" opt; do
  case $opt in
    p)
	PREFIX=$OPTARG
      ;;
    s)
	SEQFILE=$OPTARG
      ;;
    r)
	REF=$OPTARG
      ;;
    \?)
      echo "Invalid option: -$OPTARG" 
      ;;
  esac
done


#concatenate results
grep -vh '#' *_rewritten.maf >> ${PREFIX}.maf

./maf-sort ${PREFIX}.maf > ${PREFIX}_sorted.maf


#cleanup intermediate files
rm ${PREFIX}.maf
rm *joinmaflog*
rm -r splitmaf_*


#add species names
python add_speciesnames.py --prefix $PREFIX --seqfile $SEQFILE --ref $REF