# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 15:40:58 2019

@author: kruis015
"""

import sys
sys.path.append('/lustre/backup/WUR/ABGC/kruis015/scripts')
from functions import parse_maf, write_maf
import argparse

parser = argparse.ArgumentParser(description='add species names to a maf file')
parser.add_argument("--prefix", help="multi alignmer prefix", type=str)
parser.add_argument("--seqfile", help="multi aligner seqfile", type=str)
parser.add_argument("--ref", help="reference species", type=str)
args = parser.parse_args()


seqfile = args.seqfile
maf = args.prefix+'_sorted.maf'
mafout = args.prefix+'_sorted_speciesnames.maf'

def add_speciesname(parsedmaf, scafdict):
    for block in parsedmaf:
        try:
            block.reference.contig = scafdict[block.reference.contig]+'.'+block.reference.contig
        except KeyError:
            block.reference.contig = 'unknown.'+block.reference.contig
        for line in block.lines:
            try:
                line.contig = scafdict[line.contig]+'.'+line.contig
            except KeyError:
                line.contig = 'unknown.'+line.contig            
        yield block

scaflist = {}
with open(seqfile) as f:
    for line in f:
        species, fasta = line.strip().split()
        species = species.strip('*')
        faidx = fasta+'.fai'
        with open(faidx) as f2:
            for line in f2:
                scaflist[line.split()[0]] = species

parsedmaf = parse_maf(maf, args.ref)

write_maf(add_speciesname(parsedmaf, scaflist), mafout)
    